const express = require('express')
const cors = require('cors')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const mongoose = require('mongoose')

// import router
const indexRouter = require('./routes/index')
const usersRouter = require('./routes/users')
const authRouter = require('./routes/auth')
const agencyRouter = require('./routes/agency')
const bookingRouter = require('./routes/bookings')
const roomRouter = require('./routes/rooms')
const buildingRouter = require('./routes/buildings')
const facultyRouter = require('./routes/facultys')
const branchRouter = require('./routes/branch')

const dotenv = require('dotenv')
const { authenMiddleware, authorizemiddleware } = require('./helpers/auth')
const { ROLE } = require('./constant')
// get config vars
dotenv.config()

mongoose.connect('mongodb://localhost:27017/datalist')
const app = express()
app.use(cors())
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

// api router
app.use('/', indexRouter)
app.use('/users', authenMiddleware, authorizemiddleware([ROLE.LOCAL_ADMIN, ROLE.UNIT_ADMIN, ROLE.APPROVER, ROLE.USER]), usersRouter)
app.use('/agency', agencyRouter)
app.use('/auth', authRouter)
app.use('/bookings', bookingRouter)
app.use('/rooms', roomRouter)
app.use('/building', buildingRouter)
app.use('/faculty', facultyRouter)
app.use('/branch', branchRouter)
module.exports = app
