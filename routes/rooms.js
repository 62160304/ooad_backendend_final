const express = require('express')
const router = express.Router()
const Room = require('../models/Room')
// get all
const getRooms = async function (req, res, next) {
  try {
    const rooms = await Room.find({}).exec()
    res.status(200).json(rooms)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}
// get id
const getRoom = async function (req, res, next) {
  const id = req.params.id
  console.log(id)
  try {
    const room = await Room.findById(id).exec()
    if (room === null) {
      return res.status(404).json({
        message: 'Room not found!'
      })
    }
    res.json(room)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}
// getBuilding
const getBuilding = async function (req, res, next) {
  try {
    const building = await Room.find({ building: req.params.building })
    if (building === null) {
      return res.status(404).json({
        message: 'Room not found!'
      })
    }
    res.json(building)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}

const addRooms = async function (req, res, next) {
  const newRoom = new Room({
    code: req.body.code,
    dasc: req.body.dasc,
    floor: req.body.floor,
    seat: req.body.seat
  })
  try {
    await newRoom.save()
    res.status(201).json(newRoom)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateRoom = async function (req, res, next) {
  const roomId = req.params.id
  try {
    const room = await Room.findById(roomId)
    room.code = req.body.code
    room.dasc = req.body.dasc
    room.floor = req.body.floor
    room.seat = req.body.seat
    await room.save()
    return res.status(200).json(room)
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

const deleteRoom = async function (req, res, next) {
  const roomId = req.params.id
  try {
    await Room.findByIdAndDelete(roomId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/', getRooms)
router.get('/:id', getRoom)
router.get('/find/:building', getBuilding)
router.post('/', addRooms)
router.put('/:id', updateRoom)
router.delete('/:id', deleteRoom)
module.exports = router
