const express = require('express')
const router = express.Router()
const Building = require('../models/Building')

const getBuildings = async function (req, res, next) {
  try {
    const buildings = await Building.find({}).exec()
    res.status(200).json(buildings)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getBuildingCode = async function (req, res, next) {
  try {
    const building = await Building.find({ code: req.params.code }).populate('rooms')
    if (building === null) {
      return res.status(404).json({
        message: 'Building not found!'
      })
    }
    res.json(building)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}

const addBuildings = async function (req, res, next) {
  const newBuilding = new Building({
    name: req.body.name,
    code: req.body.code,
    floor: req.body.floor,
    rooms: req.body.rooms,
    agency: req.body.agency
  })
  try {
    await newBuilding.save()
    res.status(201).json(newBuilding)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateBuilding = async function (req, res, next) {
  const buildingId = req.params.id
  try {
    const building = await Building.findById(buildingId)
    building.name = req.body.name
    building.code = req.body.code
    building.floor = req.body.floor
    building.rooms = req.body.rooms
    building.agency = req.body.agency
    await building.save()
    return res.status(200).json(building)
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

const deleteBuilding = async function (req, res, next) {
  const buildingId = req.params.id
  try {
    await Building.findByIdAndDelete(buildingId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/', getBuildings)
router.get('/:code', getBuildingCode)
router.post('/', addBuildings)
router.put('/:id', updateBuilding)
router.delete('/:id', deleteBuilding)
module.exports = router
