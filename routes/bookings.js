const express = require('express')
const router = express.Router()
const Booking = require('../models/Booking')

const getBookings = async function (req, res, next) {
  try {
    const currentDate = req.query.currentDate
    const bookings = await Booking.find({
      day: {
        $gt: (currentDate),
        $lt: (currentDate)
      }
    })
    res.status(200).json(bookings)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getBooking = async function (req, res, next) {
  const id = req.params.id
  console.log(id)
  try {
    const booking = await Booking.findById(id).exec()
    if (booking === null) {
      return res.status(404).json({
        message: 'Not found!'
      })
    }
    res.json(booking)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}

const addBooking = async function (req, res, next) {
  const newBooking = new Booking({
    name: req.body.name,
    building: req.body.building,
    floor: req.body.floor,
    room: req.body.room,
    position: req.body.position,
    faculty: req.body.faculty,
    branch: req.body.branch,
    content: req.body.content,
    equipment: req.body.equipment,
    currentDate: req.body.currentDate,
    startTime: req.body.startTime,
    endTime: req.body.endTime
  })
  try {
    await newBooking.save()
    res.status(201).json(newBooking)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateBooking = async function (req, res, next) {
  const bookingId = req.params.id
  try {
    const booking = await Booking.findById(bookingId)
    booking.name = req.body.name
    booking.building = req.body.building
    booking.floor = req.body.floor
    booking.room = req.body.room
    booking.position = req.body.position
    booking.faculty = req.body.faculty
    booking.branch = req.body.branch
    booking.content = req.body.content
    booking.equipment = req.body.equipment
    booking.currentDate = req.body.currentDate
    booking.startTime = req.body.startTime
    booking.endTime = req.body.endTime
    await booking.save()
    return res.status(200).json(booking)
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

const deleteBooking = async function (req, res, next) {
  const bookingId = req.params.id
  try {
    await Booking.findByIdAndDelete(bookingId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/', getBookings)
router.get('/:id', getBooking)
router.post('/', addBooking)
router.put('/:id', updateBooking)
router.delete('/:id', deleteBooking)

module.exports = router
