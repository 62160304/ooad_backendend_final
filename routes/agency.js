const express = require('express')
const router = express.Router()
const Agency = require('../models/Agency')

const getAgencys = async function (req, res, next) {
  try {
    const agencys = await Agency.find({}).exec()
    res.status(200).json(agencys)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getAgency = async function (req, res, next) {
  const id = req.params.id
  console.log(id)
  try {
    const agency = await Agency.findById(id).exec()
    if (agency === null) {
      return res.status(404).json({
        message: 'Agency not found!'
      })
    }
    res.json(agency)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}

const addAgency = async function (req, res, next) {
  const newAgency = new Agency({
    name: req.body.name,
    mum_unit: req.body.mum_unit
  })
  try {
    await newAgency.save()
    res.status(201).json(newAgency)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const deleteAgency = async function (req, res, next) {
  const agencyId = req.params.id
  try {
    await Agency.findByIdAndDelete(agencyId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/', getAgencys)
router.get('/:id', getAgency)
router.post('/', addAgency)
router.delete('/:id', deleteAgency)

module.exports = router
