const express = require('express')
const router = express.Router()
const Branch = require('../models/Branch')

const getBranchs = async function (req, res, next) {
  try {
    const branchs = await Branch.find({}).exec()
    res.status(200).json(branchs)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getBranch = async function (req, res, next) {
  const id = req.params.id
  console.log(id)
  try {
    const branch = await Branch.findById(id).exec()
    if (branch === null) {
      return res.status(404).json({
        message: 'Branch not found!'
      })
    }
    res.json(branch)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}

const getFaculty = async function (req, res, next) {
  try {
    const faculty = await Branch.find({ faculty: req.params.faculty })
    if (faculty === null) {
      return res.status(404).json({
        message: 'Branch not found!'
      })
    }
    res.json(faculty)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}

const addBranch = async function (req, res, next) {
  const newBranch = new Branch({
    branch: req.body.branch
  })
  try {
    await newBranch.save()
    res.status(201).json(newBranch)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const deleteBranch = async function (req, res, next) {
  const branchId = req.params.id
  try {
    await Branch.findByIdAndDelete(branchId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/', getBranchs)
router.get('/:id', getBranch)
router.get('/find/:faculty', getFaculty)
router.post('/', addBranch)
router.delete('/:id', deleteBranch)

module.exports = router
