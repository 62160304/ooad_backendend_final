const express = require('express')
const router = express.Router()
const Faculty = require('../models/Faculty')

const getFacultys = async function (req, res, next) {
  try {
    const facultys = await Faculty.find({}).exec()
    res.status(200).json(facultys)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getFaculty = async function (req, res, next) {
  const id = req.params.id
  console.log(id)
  try {
    const faculty = await Faculty.findById(id).exec()
    if (faculty === null) {
      return res.status(404).json({
        message: 'Faculty not found!'
      })
    }
    res.json(faculty)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}

const getFacultyName = async function (req, res, next) {
  try {
    const faculty = await Faculty.find({ faculty: req.params.faculty }).populate('branch')
    if (faculty === null) {
      return res.status(404).json({
        message: 'Faculty not found!'
      })
    }
    res.json(faculty)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}

const addFaculty = async function (req, res, next) {
  const newFaculty = new Faculty({
    faculty: req.body.faculty
  })
  try {
    await newFaculty.save()
    res.status(201).json(newFaculty)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const deleteFaculty = async function (req, res, next) {
  const facultyId = req.params.id
  try {
    await Faculty.findByIdAndDelete(facultyId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/', getFacultys)
router.get('/:id', getFaculty)
router.get('/find/:faculty', getFacultyName)
router.post('/', addFaculty)
router.delete('/:id', deleteFaculty)

module.exports = router
