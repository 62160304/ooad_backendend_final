const mongoose = require('mongoose')
const Room = require('../models/Room')
mongoose.connect('mongodb://localhost:27017/datalist')

async function clearRoom () {
  await Room.deleteMany({})
}

async function main () {
  await clearRoom()
  await Room.insertMany([
    { code: 'IF-3C01', dasc: 'Presentter+TV+Projector', floor: '3', seat: 100, building: 'IF' },
    { code: 'IF-3C02', dasc: 'Presentter+TV+Projector', floor: '3', seat: 100, building: 'IF' },
    { code: 'IF-3C03', dasc: 'Presentter+TV+Projector', floor: '3', seat: 100, building: 'IF' },
    { code: 'IF-3C04', dasc: 'Presentter+TV+Projector', floor: '3', seat: 100, building: 'IF' },
    { code: 'SD-3C01', dasc: 'Presentter+TV+Projector', floor: '3', seat: 100, building: 'SD' },
    { code: 'SD-3C02', dasc: 'Presentter+TV+Projector', floor: '3', seat: 100, building: 'SD' },
    { code: 'SD-3C03', dasc: 'Presentter+TV+Projector', floor: '3', seat: 100, building: 'SD' },
    { code: 'STU-211', dasc: 'Presentter+TV+Projector', floor: '1', seat: 50, building: 'STU_AFF' },
    { code: 'STU-222', dasc: 'Presentter+TV+Projector', floor: '2', seat: 100, building: 'STU_AFF' }
  ])
}

main().then(function () {
  console.log('Finish')
})
