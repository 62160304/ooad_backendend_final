const mongoose = require('mongoose')
const Agency = require('../models/Agency')
mongoose.connect('mongodb://localhost:27017/datalist')
async function clearAgency () {
  await Agency.deleteMany({})
}
async function main () {
  await clearAgency()
  const agency = new Agency({ name: 'หน่วยอาคารและสถานที่ ', mum_unit: 8 })
  agency.save()
}
main().then(function () {
  console.log('Finish')
})
