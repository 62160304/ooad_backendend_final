const mongoose = require('mongoose')
const Booking = require('../models/Booking')
mongoose.connect('mongodb://localhost:27017/datalist')

async function clearBooking () {
  await Booking.deleteMany({})
}

async function main () {
  await clearBooking()
  await Booking.insertMany([
    {
      name: 'susan', building: 'IF', floor: '3', room: '3C-301', position: 'นักศึกษา', faculty: 'คณะวิทยาการสารสนเทศ', branch: 'สาขาวิชาวิทยาการคอมพิวเตอร', content: 'ส่งงาน', equipment: ['Presentter', 'TV', 'Projector'], currentDate: new Date('2022-04-10'), startTime: '08:00', endTime: '16:00'
    },
    {
      name: 'siri', building: 'SD', floor: '1', room: 'SD-101', position: 'อาจารย์', faculty: 'คณะวิทยาศาสตร์', branch: 'สาขาวิชาสถิติ', content: 'อบรม', equipment: ['TV', 'Projector'], currentDate: new Date('2022-04-12'), startTime: '08:00', endTime: '12:00'
    }
  ])
}

main().then(function () {
  console.log('Finish')
})
