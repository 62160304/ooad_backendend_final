const mongoose = require('mongoose')
const User = require('../models/User')
const { ROLE } = require('../constant')
mongoose.connect('mongodb://localhost:27017/datalist')

async function clearUser () {
  await User.deleteMany({})
}

async function main () {
  await clearUser()
  const user = new User({ username: 'user@gmail.com', password: 'us123', firstname: 'วิภาดา', lastname: 'สุขดี', position: 'นักศึกษา', roles: [ROLE.USER] })
  user.save()
  const localAdmin = new User({ username: 'localadmin@gmail.com', password: 'lo123', firstname: 'ผศ.ดร.มโน', lastname: 'สินรกำ', position: 'อาจารย์', roles: [ROLE.LOCAL_ADMIN, ROLE.USER] })
  localAdmin.save()
  const unitAdmin = new User({ username: 'unitadmin@gmail.com', password: 'un123', firstname: 'ผศ.ดร.พรนี', lastname: 'พีรโชติ', position: 'อาจารย์', roles: [ROLE.UNIT_ADMIN, ROLE.USER] })
  unitAdmin.save()
  const approver = new User({ username: 'approver@gmail.com', password: 'ap123', firstname: 'สีสัน ', lastname: 'มีอ่วม', position: 'อาจารย์', roles: [ROLE.APPROVER, ROLE.USER] })
  approver.save()
}

main().then(function () {
  console.log('Finish')
})
