const mongoose = require('mongoose')
const Building = require('../models/Building')
const Room = require('../models/Room')
mongoose.connect('mongodb://localhost:27017/datalist')

async function clearUser () {
  await Building.deleteMany({})
  await Room.deleteMany({})
}

async function main () {
  await clearUser()
  const ifBuilding = new Building({ name: 'อาคารคณะวิทยาการสารสนเทศ', code: 'IF', floor: 11, numrooms: 52, agency: 'หน่วยอาคารและสถานที่' })
  const room3co1 = new Room({ code: 'IF-3C01', dasc: 'Presentter+TV+Projector', floor: '3', seat: 100, building: ifBuilding })
  ifBuilding.rooms.push(room3co1)
  const room4co1 = new Room({ code: 'IF-4C01', dasc: 'Presentter+TV+Projector', floor: '4', seat: 100, building: ifBuilding })
  ifBuilding.rooms.push(room4co1)

  const sdBuilding = Building({ name: 'อาคารสิรินธร', code: 'SD', floor: 9, numrooms: 33, agency: 'หน่วยอาคารและสถานที่' })
  const roomsd3c01 = new Room({ code: 'SD-3C01', dasc: 'Presentter+TV+Projector', floor: '3', seat: 100, building: sdBuilding })
  sdBuilding.rooms.push(roomsd3c01)
  const roomsd3c02 = new Room({ code: 'SD-3C02', dasc: 'Presentter+TV+Projector', floor: '3', seat: 100, building: sdBuilding })
  sdBuilding.rooms.push(roomsd3c02)

  const stuBuilding = Building({ name: 'อาคารกิจกรรมนิสิต', code: 'STU_AFF', floor: 2, numrooms: 10, agency: 'หน่วยบริการและพัฒนาสังคม' })
  const roomstu211 = new Room({ code: 'STU-211', dasc: 'Presentter+TV+Projector', floor: '1', seat: 50, building: stuBuilding })
  stuBuilding.rooms.push(roomstu211)
  const roomstu222 = new Room({ code: 'STU-222', dasc: 'Presentter+TV+Projector', floor: '2', seat: 100, building: stuBuilding })
  stuBuilding.rooms.push(roomstu222)

  await ifBuilding.save()
  await room3co1.save()
  await room4co1.save()

  await sdBuilding.save()
  await roomsd3c01.save()
  await roomsd3c02.save()

  await stuBuilding.save()
  await roomstu211.save()
  await roomstu222.save()

  // await Building.insertMany([
  //   { name: 'อาคารสิรินธร', code: 'SD', floor: 9, numrooms: 33, agency: 'หน่วยอาคารและสถานที่' },
  //   { name: 'อาคารกิจกรรมนิสิต', code: 'STU_AFF', floor: 2, numrooms: 10, agency: 'หน่วยบริการและพัฒนาสังคม' },
  //   { name: 'อาคารคณะวิทยาการสารสนเทศ', code: 'IF', floor: 11, numrooms: 52, agency: 'หน่วยอาคารและสถานที่' }
  // ])
}

main().then(function () {
  console.log('Finish')
})
