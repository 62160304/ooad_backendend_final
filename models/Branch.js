const mongoose = require('mongoose')
const { Schema } = mongoose
const branchSchema = Schema({
  branch: String,
  faculty: String
})

module.exports = mongoose.model('Branch', branchSchema)
