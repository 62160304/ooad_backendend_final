const mongoose = require('mongoose')
const { Schema } = mongoose
const roomSchema = Schema({
  code: String,
  dasc: String,
  floor: String,
  seat: Number,
  building: String
})

module.exports = mongoose.model('Room', roomSchema)
