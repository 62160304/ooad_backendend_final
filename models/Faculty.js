const mongoose = require('mongoose')
const { Schema } = mongoose
const facultySchema = Schema({
  faculty: String,
  branch: [{ type: Schema.Types.ObjectId, ref: 'Branch', default: [] }]
})

module.exports = mongoose.model('Faculty', facultySchema)
