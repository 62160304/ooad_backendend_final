const mongoose = require('mongoose')
const { Schema } = mongoose
const buildingSchema = Schema({
  id: Number,
  name: String,
  code: String,
  floor: Number,
  numrooms: Number,
  agency: String,
  rooms: [{ type: Schema.Types.ObjectId, ref: 'Room', default: [] }]
})

module.exports = mongoose.model('Buildings', buildingSchema)
