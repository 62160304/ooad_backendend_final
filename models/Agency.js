const mongoose = require('mongoose')
const { Schema } = mongoose
const agencySchema = Schema({
  name: String,
  mum_unit: Number
})

module.exports = mongoose.model('Agency', agencySchema)
