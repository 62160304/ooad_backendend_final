const mongoose = require('mongoose')
const { Schema } = mongoose
const bookingSchema = Schema({
  name: String,
  building: String,
  floor: String,
  room: String,
  position: String,
  faculty: String,
  branch: String,
  content: String,
  equipment: [],
  currentDate: Date,
  startTime: String,
  endTime: String
},
{
  timestamps: true
})
module.exports = mongoose.model('Booking', bookingSchema)
